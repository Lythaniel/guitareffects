#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

import matplotlib.pyplot as plt
import matplotlib as mpl
import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import threading
import time

class Osciloscope():
    """description of class"""
    def __init__(self, master=None):
        #tk.Canvas.__init__(self, master)
        self.figure = mpl.figure.Figure(figsize=(6,6))
        self.canvas = FigureCanvasTkAgg(self.figure, master)
        self.win = master

        self.enable = False

        self.axIn = self.figure.add_subplot(211)
        self.axIn.set_title("In")
       
        self.X = range (0,1024)
        self.YIn = [0] * 1024;
        self.axIn.set_ylim (ymin = -1, ymax = 1)
        self.axIn.set_xticks ([])
        self.axIn.set_xmargin(0)
        self.axIn.spines['left'].set_position(('data', 0))
        self.axIn.spines['bottom'].set_position(('data', 0))
        self.axIn.spines['top'].set_visible(False)
        self.axIn.spines['right'].set_visible(False)

        self.plotIn, = self.axIn.plot(self.X, self.YIn)

        self.axOut = self.figure.add_subplot(212)
        self.axOut.set_title("Out")
       
        self.YOut = [0] * 1024;
        self.axOut.set_ylim (ymin = -1, ymax = 1)
        self.axOut.set_xticks ([])
        self.axOut.set_xmargin(0)
        self.axOut.spines['left'].set_position(('data', 0))
        self.axOut.spines['bottom'].set_position(('data', 0))
        self.axOut.spines['top'].set_visible(False)
        self.axOut.spines['right'].set_visible(False)

        self.plotOut, = self.axOut.plot(self.X, self.YOut)
        self.refresh()


    def pushDataIn (self, data):
        self.YIn = [y / 32767 for y in data]

    def pushDataOut (self, data):
        self.YOut = [y / 32767 for y in data]

    def refresh (self):
        if (self.enable):
            self.plotIn.set_ydata (self.YIn)
            self.plotOut.set_ydata (self.YOut)
            self.canvas.draw()
            self.canvas.flush_events()
        self.win.after (100, self.refresh)
       

 
