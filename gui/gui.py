#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

import tkinter as tk
import globals
from processings import *
from gui import osciloscope as os

class Gui(tk.Frame):
    """description of class"""
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        master.title ("GuitarEffects")
        # Input selection menu.
        menuBar = tk.Menu(master)
        master['menu'] = menuBar
        inputMenu = tk.Menu(menuBar)
        inputsLabels = ['Sinus 100Hz', 'Sinus 440Hz', 'Sinus 1kHz', 'Sinus 5kHz', 'Guitar']
      
        menuBar.add_cascade(label="Input", menu=inputMenu)
        for i in range (5):
            inputMenu.add_radiobutton(label = inputsLabels[i], variable = globals.inputsVar, value = i)

        self.gainProcFrame = tk.LabelFrame(master=self, text="Gain based process" )

        # Volume
        self.volumeFrame = tk.LabelFrame(master=self.gainProcFrame, text="Volume")
        self.volumeScale = tk.Scale(master=self.volumeFrame, from_=12, to = -80, command=self.setVolume)
        self.volumeScale.set(globals.LinearTodB(globals.volume.gain, 2))
        self.volumeFrame.grid(column = 0, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.volumeFrame.rowconfigure(0, minsize=30, weight=1)
        self.volumeScale.grid(column = 0, row = 0, sticky='s')

        # Tone control
        self.toneFrame = tk.LabelFrame(master=self.gainProcFrame, text="Tone")
        self.toneEnabled = tk.Checkbutton(master=self.toneFrame, text="Enable", variable=globals.toneEnabled)

        self.bassScale = tk.Scale(master=self.toneFrame, label = "Bass", from_=9, to = -9, command=self.setBass)
        self.medScale = tk.Scale(master=self.toneFrame, label = "Med", from_=9, to = -9, command=self.setMed)
        self.trebbleScale = tk.Scale(master=self.toneFrame, label = "Trebble", from_=9, to = -9, command=self.setTrebble)
        self.bassScale.set(globals.tone.bass-10)
        self.medScale.set(globals.tone.med-10)
        self.trebbleScale.set(globals.tone.trebble-10)
        self.toneFrame.grid(column = 2, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.toneEnabled.grid(column = 0, row = 0)
        self.bassScale.grid(column = 0, row = 1)
        self.medScale.grid(column = 1, row = 1)
        self.trebbleScale.grid(column = 2, row = 1)
        
        
        # Distortion
        self.distFrame = tk.LabelFrame(master=self.gainProcFrame, text="Distortion")
        self.distEnabled = tk.Checkbutton(master=self.distFrame, text="Enable", variable=globals.distEnabled)

        self.distGain = tk.Scale(master=self.distFrame, label = "Gain", from_=36, to = 0, command=self.setDistGain)
        self.distGain.set(globals.LinearTodB(globals.dist.gain,6))
        self.distClip = tk.Scale(master=self.distFrame, label = "Clip", from_= 1, to = 0, resolution = 0.1, command=self.setDistClip)
        self.distClip.set(globals.dist.clip/32767)

        self.distFrame.grid(column = 5, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.distEnabled.grid(column = 0, row = 0)
        self.distGain.grid(column = 0, row = 1)
        self.distClip.grid(column = 1, row = 1)

        # Overdrive
        self.odFrame = tk.LabelFrame(master=self.gainProcFrame, text="Overdrive")
        self.odEnabled = tk.Checkbutton(master=self.odFrame, text="Enable", variable=globals.odEnabled)

        self.odGainDry = tk.Scale(master=self.odFrame, label = "Mix", from_=100, to = 0, command=self.setOdMix)
        self.odGainDry.set(globals.od.mix / 32767 * 100)
        self.odGainWet = tk.Scale(master=self.odFrame, label = "Wet Gain", from_=30, to = 0, command=self.setOdGainWet)
        self.odGainWet.set(globals.LinearTodB(globals.od.gainWet,5))

        self.odFrame.grid(column = 7, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.odEnabled.grid(column = 0, row = 0)
        self.odGainDry.grid(column = 0, row = 1)
        self.odGainWet.grid(column = 1, row = 1)

        self.gainProcFrame.grid(column = 0, row = 0, columnspan = 8, sticky='nesw', padx = 10, pady = 10)


        self.delayProcFrame = tk.LabelFrame(master=self, text="Delay based process")

        # Delay
        self.delayFrame = tk.LabelFrame(master=self.delayProcFrame, text="Delay")
        self.delayEnabled = tk.Checkbutton(master=self.delayFrame, text="Enable", variable=globals.delayEnabled)

        self.delayDelay = tk.Scale(master=self.delayFrame, label = "Delay", from_= 1000, to = 0, command=self.setDelayDelay)
        self.delayDelay.set(globals.delay.delay / 44.1)

        self.delayGain = tk.Scale(master=self.delayFrame, label = "Gain", from_= 0, to = -30, command=self.setDelayGain)
        self.delayGain.set(globals.LinearTodB(globals.delay.gain,0))

        self.delayFrame.grid(column = 0, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.delayEnabled.grid(column = 0, row = 0)
        self.delayDelay.grid(column = 0, row = 1)
        self.delayGain.grid(column = 1, row = 1)

        # Echo

        self.echoFrame = tk.LabelFrame(master=self.delayProcFrame, text="Echo")
        self.echoEnabled = tk.Checkbutton(master=self.echoFrame, text="Enable", variable=globals.echoEnabled)

        self.echoDelay = tk.Scale(master=self.echoFrame, label = "Delay", from_= 1000, to = 0, command=self.setEchoDelay)
        self.echoDelay.set(int(globals.echo.delay / 44.1))

        self.echoGain = tk.Scale(master=self.echoFrame, label = "Gain", from_= 0, to = -30, command=self.setEchoGain)
        self.echoGain.set(globals.LinearTodB(globals.echo.gain,0))

        self.echoFrame.grid(column = 1, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.echoEnabled.grid(column = 0, row = 0)
        self.echoDelay.grid(column = 0, row = 1)
        self.echoGain.grid(column = 1, row = 1)

        self.delayProcFrame.grid (column = 0, row = 1, columnspan = 8, sticky='nesw', padx = 10, pady = 10)
        
        
        self.modProcFrame = tk.LabelFrame(master=self, text="Modulation based process")

        # Tremolo
        self.tremoloFrame = tk.LabelFrame(master=self.modProcFrame, text="Tremolo")
        self.tremoloEnabled = tk.Checkbutton(master=self.tremoloFrame, text="Enable", variable=globals.tremoloEnabled)

        self.tremoloFrequency = tk.Scale(master=self.tremoloFrame, label = "Freq", from_= 20, to = 1, resolution= 0.5, command=self.setTremoloFreq)
        self.tremoloFrequency.set(globals.tremolo.Freq)

        self.tremoloGain = tk.Scale(master=self.tremoloFrame, label = "Level", from_= 100, to = 0, command=self.setTremoloGain)
        self.tremoloGain.set(globals.tremolo.gain / 16384 * 100)

        self.tremoloFrame.grid(column = 0, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.tremoloEnabled.grid(column = 0, row = 0)
        self.tremoloFrequency.grid(column = 0, row = 1)
        self.tremoloGain.grid(column = 1, row = 1)


        # Chorus
        self.chorusFrame = tk.LabelFrame(master=self.modProcFrame, text="Chorus")
        self.chorusEnabled = tk.Checkbutton(master=self.chorusFrame, text="Enable", variable=globals.chorusEnabled)

        self.chorus1Frequency = tk.Scale(master=self.chorusFrame, label = "Freq 1", from_= 20, to = 0.1, resolution= 0.1, command=self.setChorus1Freq)
        self.chorus1Frequency.set(globals.chorus.chorusTap1.Freq)

        self.chorus1Delay = tk.Scale(master=self.chorusFrame, label = "Delay 1", from_= 100, to = 0, command=self.setChorus1Delay)
        self.chorus1Delay.set(globals.chorus.chorusTap1.delay / 44)

        self.chorus1DelayDepth = tk.Scale(master=self.chorusFrame, label = "Depth 1", from_= 500, to = 0, command=self.setChorus1DelayDepth)
        self.chorus1DelayDepth.set(globals.chorus.chorusTap1.delaydepth )

        self.chorus1Gain = tk.Scale(master=self.chorusFrame, label = "Level 1", from_= 0, to = -30, command=self.setChorus1Gain)
        self.chorus1Gain.set(globals.LinearTodB(globals.chorus.chorusTap1.gain,0))

        self.chorus2Frequency = tk.Scale(master=self.chorusFrame, label = "Freq 2", from_= 20, to = 0.1, resolution= 0.1, command=self.setChorus2Freq)
        self.chorus2Frequency.set(globals.chorus.chorusTap2.Freq)

        self.chorus2Delay = tk.Scale(master=self.chorusFrame, label = "Delay 2", from_= 100, to = 0, command=self.setChorus2Delay)
        self.chorus2Delay.set(globals.chorus.chorusTap2.delay / 44)

        self.chorus2DelayDepth = tk.Scale(master=self.chorusFrame, label = "Depth 2", from_= 500, to = 0, command=self.setChorus2DelayDepth)
        self.chorus2DelayDepth.set(globals.chorus.chorusTap2.delaydepth)

        self.chorus2Gain = tk.Scale(master=self.chorusFrame, label = "Level 2", from_= 0, to = -30, command=self.setChorus2Gain)
        self.chorus2Gain.set(globals.LinearTodB(globals.chorus.chorusTap2.gain,0))

        self.chorusFrame.grid(column = 0, row = 1, columnspan = 8, padx = 5, pady = 5, sticky='nesw')
        self.chorusEnabled.grid(column = 0, row = 0)
        self.chorus1Frequency.grid(column = 0, row = 1)
        self.chorus1Delay.grid(column = 1, row = 1)
        self.chorus1DelayDepth.grid(column = 2, row = 1)
        self.chorus1Gain.grid(column = 3, row = 1)
        self.chorus2Frequency.grid(column = 4, row = 1)
        self.chorus2Delay.grid(column = 5, row = 1)
        self.chorus2DelayDepth.grid(column = 6, row = 1)
        self.chorus2Gain.grid(column = 7, row = 1)


        # Flanger
        self.flangerFrame = tk.LabelFrame(master=self.modProcFrame, text="flanger")
        self.flangerEnabled = tk.Checkbutton(master=self.flangerFrame, text="Enable", variable=globals.flangerEnabled)

        self.flangerFrequency = tk.Scale(master=self.flangerFrame, label = "Freq", from_= 20, to = 0.1, resolution= 0.1, command=self.setFlangerFreq)
        self.flangerFrequency.set(globals.flanger.Freq)

        self.flangerDelay = tk.Scale(master=self.flangerFrame, label = "Delay", from_= 100, to = 0, command=self.setFlangerDelay)
        self.flangerDelay.set(globals.flanger.delay / 44)

        self.flangerDelayDepth = tk.Scale(master=self.flangerFrame, label = "Delay depth", from_= 500, to = 0, command=self.setFlangerDelayDepth)
        self.flangerDelayDepth.set(globals.flanger.delaydepth)

        self.flangerGain = tk.Scale(master=self.flangerFrame, label = "Level", from_= 0, to = -30, command=self.setFlangerGain)
        self.flangerGain.set(globals.LinearTodB(globals.flanger.gain,0))

        self.flangerFeedbackGain = tk.Scale(master=self.flangerFrame, label = "Feedback", from_= 0, to = -30, command=self.setFlangerFeedbackGain)
        self.flangerFeedbackGain.set(globals.LinearTodB(globals.flanger.feedbackGain,0))

        self.flangerFrame.grid(column = 1, row = 0, padx = 5, pady = 5, sticky='nesw')
        self.flangerEnabled.grid(column = 0, row = 0)
        self.flangerFrequency.grid(column = 0, row = 1)
        self.flangerDelay.grid(column = 1, row = 1)
        self.flangerDelayDepth.grid(column = 2, row = 1)
        self.flangerGain.grid(column = 3, row = 1)
        self.flangerFeedbackGain.grid(column = 4, row = 1)

        self.modProcFrame.grid (column = 0, row = 2, columnspan = 8, padx = 10, pady = 10)
            
        self.scopeFrame = tk.LabelFrame(master=self, text="Scope")
        self.scopeEnable = tk.Checkbutton(master=self.scopeFrame, text="Enable", command=self.setScopeEnable, offvalue = False, onvalue = True, variable = globals.scopeEnabled)
        self.scope = os.Osciloscope(master=self.scopeFrame)
        self.scopeEnable.grid(column = 0, row = 0)
        self.scope.canvas.get_tk_widget().grid(column = 0, row = 1)
        self.scopeFrame.grid(column = 9, row = 0, rowspan = 4, padx = 10, pady = 10)

        self.grid()

    def setVolume (self, gain):
        vol = globals.dBToLinear(int(gain),2)
        globals.volume.gain = vol

    def setBass (self, gain):
        lev = int(gain) + 9
        globals.tone.bass = lev

    def setMed (self, gain):
        lev = int(gain) + 9
        globals.tone.med = lev

    def setTrebble (self, gain):
        lev = int(gain) + 9
        globals.tone.trebble = lev

    def setDistGain (self, gain):
        vol = globals.dBToLinear(int(gain),6)
        globals.dist.gain = int(vol)

    def setDistClip (self, clip):
        globals.dist.clip = int(float(clip) * 32767)

    def setOdMix (self, gain):
        globals.od.mix = int(float(gain)/100 *32767)
    
    def setOdGainWet (self, gain):
        vol = globals.dBToLinear(int(gain),5)
        globals.od.gainWet = int(vol)

    def setDelayDelay (self, delay):
        globals.delay.setDelay (int(float(delay) * 44.1))

    def setDelayGain(self, gain):
        vol = globals.dBToLinear(int(gain),0)
        globals.delay.gain = int(vol)

    def setEchoDelay (self, delay):
        globals.echo.setDelay (int(float(delay) * 44.1))

    def setEchoGain(self, gain):
        vol = globals.dBToLinear(int(gain),0)
        globals.echo.gain = int(vol)

    def setTremoloFreq(self, freq):
        globals.tremolo.setFrequency(float(freq))

    def setTremoloGain(self, gain):
        globals.tremolo.gain = int(float(gain) * 16384 / 100)

    def setChorus1Freq(self, freq):
        globals.chorus.chorusTap1.setFrequency(float(freq))

    def setChorus1Delay (self, delay):
        globals.chorus.chorusTap1.setDelay(int(float(delay) * 44.1))

    def setChorus1DelayDepth (self, delay):
        globals.chorus.chorusTap1.delaydepth = int(delay)

    def setChorus1Gain(self, gain):
        vol = globals.dBToLinear(int(gain),0)
        globals.chorus.chorusTap1.gain = int(vol)

    def setChorus2Freq(self, freq):
        globals.chorus.chorusTap2.setFrequency(float(freq))

    def setChorus2Delay (self, delay):
        globals.chorus.chorusTap2.setDelay(int(float(delay) * 44.1))

    def setChorus2DelayDepth (self, delay):
        globals.chorus.chorusTap2.delaydepth = int(delay)

    def setChorus2Gain(self, gain):
        vol = globals.dBToLinear(int(gain),0)
        globals.chorus.chorusTap2.gain = int(vol)

    def setFlangerFreq(self, freq):
        globals.flanger.setFrequency(float(freq))

    def setFlangerDelay (self, delay):
        globals.flanger.setDelay(int(float(delay) * 44.1))

    def setFlangerDelayDepth (self, delay):
        globals.flanger.delaydepth = int(delay)

    def setFlangerGain(self, gain):
        vol = globals.dBToLinear(int(gain),0)
        globals.flanger.gain = int(vol)

    def setFlangerFeedbackGain(self, gain):
        vol = globals.dBToLinear(int(gain),0)
        globals.flanger.feedbackGain = int(vol)

    def setScopeEnable (self):
        self.scope.enable = globals.scopeEnabled.get()





