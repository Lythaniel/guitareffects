#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from array import *
import math

size = (1 <<  12) # 4096. size must be a power of 2.
_mask = size - 1 # mask for circular buffer.
_table = array ('h', [0] *  size) #sinus table.
#generate sinus table.
for i in range (0,size):
        _table[i] = int (math.sin(i/size * 2 * math.pi) * 32767) 


def sin (phase, step, len):
    #init output array.
    out = array ('h', [0] *  len)
    for i in range (0,len):
        #convert phase to circular buffer index.
        iphase = int(phase) & _mask
        #get value from sinus table
        out[i] = _table[iphase]
        #increment phase
        phase += step
    return (out, phase)
