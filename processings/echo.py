#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from array import *

class Echo():
    def __init__(self):
        self.delay = 441 * 10 #initial delay.
        self.delaySz = (1 << 16) #65536: 1.48s max delay size of the delay line. Use a power of 2 !
        self.delayMsk = self.delaySz - 1 #mask used for circular buffer index calculation. 
        self.delayline = array ('h', [0] * self.delaySz) #delay line
        
        self.wrPtr = self.delay; #write pointer
        self.rdPtr = 0; #read pointer
        self.gain = 0x4000 #feedback gain.

    def process (self, audio):
        for i in range (0,len(audio)):
            #calculate new output as the audio input + delay line output.
            out = (audio[i] + ((self.delayline[self.rdPtr] * self.gain) >> 15))
            #clip
            if (out > 32767): out = 32767
            elif (out < -32767): out = -32767

            #store output in delay line.
            self.delayline[self.wrPtr] = out
            audio[i] = out

            #update delay line pointer.
            self.wrPtr = (self.wrPtr + 1) & self.delayMsk;
            self.rdPtr = (self.rdPtr + 1) & self.delayMsk;
        return audio

    def setDelay (self, delay):
        #sanity check
        if (delay > self.delaySz):
            delay = self.delaySz
        #update read pointer with new delay.
        self.rdPtr = (self.wrPtr - delay) & self.delayMsk