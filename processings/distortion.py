#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from array import *

class Distortion():
	def __init__(self):
		self.gain = 0x7000
		self.clip = 0x7FFF


	def process (self, audio):
		for i in range (0,len(audio)):
			
            #apply heavy gain (max 30dB)
			out = (audio[i] * self.gain) >> 9

            #clip output
			if (out > self.clip): out = self.clip
			elif (out < -self.clip): out = -self.clip
			audio[i] = out

		return audio

