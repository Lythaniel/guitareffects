#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from array import *
from processings import sinus
import math

class Tremolo():
    def __init__(self):
        self.Freq = 10 
        self.phaseStep = self.Freq * sinus.size / 44100  # Here the sinus table size is equivalent of 2 * pi.
        self.phase = 0 # initial phase
        self.gain = 0x2000

    def process (self, audio):
        #generate sinus.
        sin, self.phase = sinus.sin (self.phase, self.phaseStep, len(audio))
        for i in range (0,len(audio)):
            # we want the gain to be maximum between 0 and 1 with an average value of 0.5.
            gain = 0x4000 + ((self.gain * sin[i])>>15)
            # apply gain.
            out = (audio[i] * gain) >> 15
            #clip & output
            if (out > 32767): out = 32767
            elif (out < -32767): out = -32767
            audio[i] = out
        return audio

    def setFrequency (self, freq):
        self.Freq = freq
        self.phaseStep = self.Freq * sinus.size / 44100

        

