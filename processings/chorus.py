#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from array import *
from processings import sinus

class ChorusTap():
    def __init__(self,delay, delaydepth, freq, gain):
        self.delay = delay # delay
        self.delaydepth = delaydepth # maximum delay excursion. max applied delay will be delay + delaydepth
        self.delaySz = (1 << 16) #65536: 1.48s max delay. size of the delay line. Use a power of 2 !
        self.delayMsk = self.delaySz - 1 #mask used for circular buffer index calculation.
        self.delayline = array ('h', [0] * self.delaySz) #delay line
        
        self.wrPtr = self.delay; #write pointer
        self.rdPtr = 0; #read pointer

        self.Freq = freq #delay modulation frequency
        self.phaseStep = self.Freq * sinus.size / 44100 #phase increment.
        self.phase = 0 #initial phase
        self.gain = gain #output gain.

    def process (self, audio):
        audioout = array ('h', [0] * len(audio))
        sin, self.phase = sinus.sin (self.phase, self.phaseStep, len(audio))
        for i in range (0,len(audio)):
            #store new sample in delay line.
            self.delayline[self.wrPtr] = audio[i]
            #calculate new delay.
            rdPtrMod = (self.delaydepth * ( 0x7FFF - sin[i])) >> 15
            #calculate circular buffer index.
            rdPtrMod = (self.rdPtr - rdPtrMod) &  self.delayMsk
            #read output & apply gain.
            audioout[i] = ((self.delayline[rdPtrMod] * self.gain) >> 15)
            #update pointers.
            self.wrPtr = (self.wrPtr + 1) & self.delayMsk;
            self.rdPtr = (self.rdPtr + 1) & self.delayMsk;
        return audioout

    def setFrequency (self, freq):
        self.Freq = freq
        self.phaseStep = self.Freq * sinus.size / 44100

    def setDelay (self, delay):
        if (delay > self.delaySz):
            delay = self.delaySz
        self.rdPtr = (self.wrPtr - delay) & self.delayMsk

class Chorus():
    def __init__(self):
        self.chorusTap1 = ChorusTap(delay=44*5, delaydepth = 20, freq = 1, gain = 0x6000)
        self.chorusTap2 = ChorusTap(delay=44*10, delaydepth = 100, freq = 0.8, gain = 0x4000)
        

    def process (self, audio):
        #calculate output for each taps.
        chorus1audio = self.chorusTap1.process (audio)
        chorus2audio = self.chorusTap2.process (audio)
        for i in range (0,len(audio)):
            #mix intput with taps output
            out = audio[i] + chorus1audio[i] + chorus2audio[i]
            #clip & output.
            if (out > 32767): out = 32767
            elif (out < -32767): out = -32767
            audio[i] = out
        return audio


  
        