#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************


# For more details on filter calculation: https://arachnoid.com/BiQuadDesigner/

from array import *
import math
from dataclasses import dataclass

@dataclass
class filterCoeffs:
    b0: int = 16384
    b1: int = 0
    b2: int = 0
    a1: int = 0
    a2: int = 0

class Tone():
    def __init__(self):
        self.medFreq = float(1000)
        self.medQ = float (0.3)

        self.bassFreq = float(400)

        self.trebFreq = float(4000)

        self._bassFilters= [];
        self._bassX1 = 0
        self._bassX2 = 0
        self._bassY1 = 0
        self._bassY2 = 0
        self._medFilters= [];
        self._medX1 = 0
        self._medX2 = 0
        self._medY1 = 0
        self._medY2 = 0
        self._trebFilters= [];
        self._trebX1 = 0
        self._trebX2 = 0
        self._trebY1 = 0
        self._trebY2 = 0
        self.trebble = 10
        self.med = 10
        self.bass = 10
        self.genBassFilters()
        self.genMedFilters()
        self.genTrebFilters()
       
   
    def genMedFilters(self):
        omega = 2 * math.pi * self.medFreq / 44100
        sn = math.sin(omega)
        cs = math.cos(omega)
        alpha = sn / (2*self.medQ)

        for dbGain in range (-9,10,1):
            A = math.pow(10, dbGain / 40) 
            a0 = (1 + (alpha /A))
            b0 = (1 + (alpha * A)) / a0
            b1 = (-2 * cs) / a0
            b2 = (1 - (alpha * A)) / a0
            a1 = (-2 * cs) / a0
            a2 = (1 - (alpha /A)) / a0

            self._medFilters.append (filterCoeffs(int(b0 * 16384), int(b1 * 16384), int(b2 * 16384), int(a1 * 16384), int(a2 * 16384)))
            

    def genBassFilters(self):
        omega = 2 * math.pi * self.bassFreq / 44100
        sn = math.sin(omega)
        cs = math.cos(omega)
        for dbGain in range (-9,10,1):
            A = math.pow(10, dbGain / 40)
            beta = math.sqrt(A + A)
            a0 = (A + 1) + (A - 1) * cs + beta * sn
            b0 = (A * ((A + 1) - (A - 1) * cs + beta * sn)) / a0
            b1 = (2 * A * ((A - 1) - (A + 1) * cs) / a0)
            b2 = (A * ((A + 1) - (A - 1) * cs - beta * sn)) / a0
            a1 = (-2 * ((A - 1) + (A + 1) * cs)) / a0
            a2 = ((A + 1) + (A - 1) * cs - beta * sn) /a0

            self._bassFilters.append (filterCoeffs(int(b0 * 16384), int(b1 * 16384), int(b2 * 16384), int(a1 * 16384), int(a2 * 16384)))

    def genTrebFilters(self):
        omega = 2 * math.pi * self.trebFreq / 44100
        sn = math.sin(omega)
        cs = math.cos(omega)
        for dbGain in range (-9,10,1):
            A = math.pow(10, dbGain / 40)
            beta = math.sqrt(A + A)
            a0 = ((A + 1) - (A - 1) * cs + beta * sn)
            b0 = (A * ((A + 1) + (A - 1) * cs + beta * sn)) / a0
            b1 = (-2 * A * ((A - 1) + (A + 1) * cs)) / a0
            b2 = (A * ((A + 1) + (A - 1) * cs - beta * sn)) / a0
            a1 = (2 * ((A - 1) - (A + 1) * cs)) / a0
            a2 = ((A + 1) - (A - 1) * cs - beta * sn) /a0
         
            self._trebFilters.append (filterCoeffs(int(b0 * 16384), int(b1 * 16384), int(b2 * 16384), int(a1 * 16384), int(a2 * 16384)))



    def process (self, audio):

        #select active filter coefficient set for bass
        bassb0 = self._bassFilters[self.bass].b0
        bassb1 = self._bassFilters[self.bass].b1
        bassb2 = self._bassFilters[self.bass].b2
        bassa1 = self._bassFilters[self.bass].a1
        bassa2 = self._bassFilters[self.bass].a2

        #select active filter coefficient set for medium
        medb0 = self._medFilters[self.med].b0
        medb1 = self._medFilters[self.med].b1
        medb2 = self._medFilters[self.med].b2
        meda1 = self._medFilters[self.med].a1
        meda2 = self._medFilters[self.med].a2

        #select active filter coefficient set for trebble
        trebb0 = self._trebFilters[self.trebble].b0
        trebb1 = self._trebFilters[self.trebble].b1
        trebb2 = self._trebFilters[self.trebble].b2
        treba1 = self._trebFilters[self.trebble].a1
        treba2 = self._trebFilters[self.trebble].a2


        for i in range (0,len(audio)):

                #calculate filter output for bass.
                x = audio[i]
                acc = (bassb0 * x) >> 15
                acc += (bassb1 * self._bassX1) >> 15
                acc += (bassb2 * self._bassX2) >> 15
                acc -= (bassa1 * self._bassY1) >> 15
                acc -= (bassa2 * self._bassY2) >> 15
                y = acc << 1

                #clip output.
                if (y > 32767): y = 32767
                elif (y < -32767): y = -32767

                #store delay result for next calculation.
                self._bassX2 = self._bassX1
                self._bassX1 = x
                self._bassY2 = self._bassY1
                self._bassY1 = y

                #calculate filter output for medium.
                x = y
                acc = (medb0 * x) >> 15
                acc += (medb1 * self._medX1) >> 15
                acc += (medb2 * self._medX2) >> 15
                acc -= (meda1 * self._medY1) >> 15
                acc -= (meda2 * self._medY2) >> 15
                y = acc << 1
                
                #clip output.
                if (y > 32767): y = 32767
                elif (y < -32767): y = -32767

                #store delay result for next calculation.
                self._medX2 = self._medX1
                self._medX1 = x
                self._medY2 = self._medY1
                self._medY1 = y

                #calculate filter output for trebble.
                x = y
                acc = (trebb0 * x) >> 15
                acc += (trebb1 * self._trebX1) >> 15
                acc += (trebb2 * self._trebX2) >> 15
                acc -= (treba1 * self._trebY1) >> 15
                acc -= (treba2 * self._trebY2) >> 15
                y = acc << 1

                 #clip output.
                if (y > 32767): y = 32767
                elif (y < -32767): y = -32767

                #store delay result for next calculation.
                self._trebX2 = self._trebX1
                self._trebX1 = x
                self._trebY2 = self._trebY1
                self._trebY1 = y

                audio[i] = y

        return audio

