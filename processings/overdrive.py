#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************


from array import *
import math

class Overdrive():
    def __init__(self):
        self.gain_curve_float = []
        self.mix = 0x7FFF  #mix value between dry and wet path.
        self.gainWet = 0x2000

        #generate gain curve. Size is a power of two to simplify the calculation later.
        for i in range (1,257):
            self.gain_curve_float.append (((math.atan( i/256 * 1.5)) / (i / 256 * 1.5)))

        #scale the curve gain.
        min_gain = self.gain_curve_float[255]
        self.gain_curve = [ (int(y / min_gain * 16383)) for y in self.gain_curve_float]


    def process (self, audio):
        for i in range (0,len(audio)):
            #add some gain on the input.          
            wet = (audio[i] * self.gainWet)  >> 10
            #clip the result
            if (wet > 32767): wet = 32767
            elif (wet < -32767): wet = -32767 # -32768 will create an index out of range.
            
            #calculate which gain to apply.
            idx = abs(wet) >> 7

            #apply gain.
            wet = (wet * self.gain_curve[idx]) >> 14
            
            #output is a mix between dry path (not modified) and the wet path (distortion added).
            out = ((audio[i] * (0x7FFF - self.mix))>>15) + ((wet * self.mix) >> 15)
            
            #clip output.
            if (out > 32767): out = 32767
            elif (out < -32767): out = -32767
            audio[i] = out

        return audio

