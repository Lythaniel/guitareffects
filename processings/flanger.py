#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from array import *
from processings import sinus

class Flanger():
    def __init__(self):
        self.delay = 44 * 10 # Delay.
        self.delaydepth = 44 * 1 # maximum delay excursion. max applied delay will be delay + delaydepth
        self.delaySz = (1 << 16) #65536: 1.48s max delay. size of the delay line. Use a power of 2 !
        self.delayMsk = self.delaySz - 1 #mask used for circular buffer index calculation. 
        self.delayline = array ('h', [0] * self.delaySz) #delay line
        self.wrPtr = self.delay #write pointer
        self.rdPtr = 0 #read pointer
        self.Freq = 1 #delay modulation frequency
        self.phaseStep = self.Freq * sinus.size / 44100 #phase increment.
        self.phase = 0 #initial phase
        self.gain = 0x4000 #mix gain.
        self.feedbackGain = 0x3000 #feed back gain.

    def process (self, audio):
        #generate sinus table.
        sin, self.phase = sinus.sin (self.phase, self.phaseStep, len(audio))
        for i in range (0,len(audio)):
            #Calculate new delay
            rdPtrMod = (self.delaydepth * ( 0x7FFF - sin[i])) >> 15
            #calculate new read pointer.
            rdPtrMod = (self.rdPtr - rdPtrMod) &  self.delayMsk
            #mix delay line output with input signal.
            out = audio[i]  + ((self.delayline[rdPtrMod] * self.gain) >> 15)
            #clip
            if (out > 32767): out = 32767
            elif (out < -32767): out = -32767
            #calculate new value to store into the delay line.
            delayin = audio[i] + ((self.delayline[rdPtrMod] * self.feedbackGain) >> 15)
            if (delayin > 32767): delayin = 32767
            elif (delayin < -32767): delayin = -32767
            # Store & update pointer.
            self.delayline[self.wrPtr] = delayin
            self.wrPtr = (self.wrPtr + 1) & self.delayMsk;
            self.rdPtr = (self.rdPtr + 1) & self.delayMsk;

            audio[i] = out
        return audio

    def setFrequency (self, freq):
        self.Freq = freq
        self.phaseStep = self.Freq * sinus.size / 44100

    def setDelay (self, delay):
        if (delay > self.delaySz):
            delay = self.delaySz
        self.rdPtr = (self.wrPtr - delay) & self.delayMsk