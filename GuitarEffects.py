#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

import pyaudio
import wave
import time
import sys
from array import *
from processings import *
from gui import gui
import globals


# Open all wave files.
fileName = ["sinus_100Hz.wav", "sinus_440Hz.wav", "sinus_1kHz.wav", "sinus_5kHz.wav", "guitar.wav"]

wf = []
for i in range (5):
    wf.append (wave.open(fileName[i], 'rb'))
    

# instantiate PyAudio (1)
p = pyaudio.PyAudio()


#Create gui
gui = gui.Gui(globals.root)


# Process loop
def process (audio):

    globals.volume.process(audio)
    if (globals.toneEnabled.get()):
        audio = globals.tone.process(audio)
    if (globals.distEnabled.get()):
        audio = globals.dist.process(audio)
    if (globals.odEnabled.get()):
        audio = globals.od.process(audio)
    if (globals.delayEnabled.get()):
        audio = globals.delay.process(audio)
    if (globals.echoEnabled.get()):
        audio = globals.echo.process(audio)
    if (globals.tremoloEnabled.get()):
        audio = globals.tremolo.process(audio)
    if (globals.chorusEnabled.get()):
        audio = globals.chorus.process(audio)
    if (globals.flangerEnabled.get()):
        audio = globals.flanger.process(audio)

    return audio

# Audio callback. will be called by PyAudio every times we need to provide new samples.
def callback(in_data, frame_count, time_info, status):
    #get active input
    input = globals.inputsVar.get()
    
    #read input. In case we reach the end of file, we rewing to the start.
    data = wf[input].readframes(frame_count)
    if (len(data) < (frame_count*2)):
        wf[input].rewind()
        data =  wf[input].readframes(frame_count)

    #convert data to array
    audio = array('h',data)

    #display input
    gui.scope.pushDataIn(audio.tolist())
    
    #Do the funny stuff
    audio = process (audio)

    #display output
    gui.scope.pushDataOut(audio.tolist())
    
    #return the new samples.
    return (audio.tobytes(), pyaudio.paContinue)


# open stream using callback
stream = p.open(format=p.get_format_from_width(wf[0].getsampwidth()),
                channels=wf[0].getnchannels(),
                rate=wf[0].getframerate(),
                output=True,
                stream_callback=callback, frames_per_buffer=1024)

# start the stream
stream.start_stream()

# start gui
gui.mainloop()

# stop stream. For some reason, this will time out so just jump it.
#stream.stop_stream()
#stream.close()

#close wave file
for i in range (5):
    wf[i].close()

# close PyAudio
p.terminate()



