#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ***************************************************************************
# *   Copyright (C) 2019, Cyrille Potereau                                  *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************

from processings import *
from tkinter import Tk, Label, Entry, Button, StringVar, IntVar
import math

root = Tk()

inputsVar = IntVar()
inputsVar.set(4) # Default is guitar

volume = volume.Volume()

toneEnabled = IntVar()
toneEnabled.set(0)
tone = tone.Tone()

distEnabled = IntVar()
distEnabled.set(0)
dist = distortion.Distortion()

odEnabled = IntVar()
odEnabled.set(0)
od = overdrive.Overdrive()

delayEnabled = IntVar()
delayEnabled.set(0)
delay = delay.Delay()

echoEnabled = IntVar()
echoEnabled.set(0)
echo = echo.Echo()

tremoloEnabled = IntVar()
tremoloEnabled.set(0)
tremolo = tremolo.Tremolo()

chorusEnabled = IntVar()
chorusEnabled.set(0)
chorus = chorus.Chorus()

flangerEnabled = IntVar()
flangerEnabled.set(0)
flanger = flanger.Flanger()

scopeEnabled = IntVar()
scopeEnabled.set(False)


def dBToLinear (db, shift):
    log = (db - (6 * shift))/20
    fract = math.pow(10,log)
    res = int (fract*32768)
    if (res > 32767): res = 32767
    return res

def LinearTodB (lin, shift):
    res = int(20*math.log10(lin/32768)) + (6 * shift)
    return res
    

